import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wedding/src/views/pages/category_page.dart';
import 'package:wedding/src/views/pages/write_review_page.dart';
import 'package:wedding/src/views/pages/favourite_page.dart';
import 'package:wedding/src/views/pages/find_friend_page.dart';
import 'package:wedding/src/views/pages/home.dart';
import 'package:wedding/src/views/pages/reviews_page.dart';

import 'src/views/pages/find_friend_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo test ci,test tiep',
      theme: ThemeData(
          primarySwatch: Colors.pink,
          appBarTheme: AppBarTheme(
              color: Colors.transparent,
              elevation: 0,
              iconTheme: IconThemeData(color: Colors.black),
              textTheme: TextTheme(
                  title: TextStyle(color: Colors.black, fontSize: 20)))),
      home: HomePage(),
    );
  }
}
