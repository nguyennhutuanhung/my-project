import 'package:flutter/material.dart';

class ThemeConfig {
  static const Color mainTitleTextColor = Color(0xff3E3F68);
  static const Color mainPinkColor = Color(0xffFF225E);
  static TextStyle titleStyle =
      TextStyle(color: mainTitleTextColor, fontWeight: FontWeight.bold);

  static TextStyle titleStyleWith(Color color) {
    return TextStyle(color: color, fontWeight: FontWeight.bold);
  }
}
