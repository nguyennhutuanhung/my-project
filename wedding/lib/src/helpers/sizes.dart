import 'package:flutter/material.dart';

class Sizes {
  //do not call constructor to create new instance

  Sizes._(BuildContext context) {
    _context = context;
  }

  static Sizes _sizes;

  static Sizes set(BuildContext context) {
    _sizes ??= Sizes._(context);
    _sizes._context = context;
    return _sizes;
  }

  BuildContext _context;

  ///get width of fullscreen by percent
  double getW([double percent = 100]) {
    //lấy kích size dựa trên tỉ lệ của màn hình.
    //value là tỉ lệ phần trăm so với màn hình,
    // ví dụ value = 10, width = 400 =>  tương ứng 10% của 400 = 40(size)
    return percent / 100.0 * MediaQuery.of(_context).size.width;
  }

  ///get height of fullscreen(contains appbar and bottom bar) by percent
  double getH([double percent = 100]) {
    //lấy kích size dựa trên tỉ lệ của màn hình.
    //value là tỉ lệ phần trăm so với màn hình,
    // ví dụ value = 10, width = 400 =>  tương ứng 10% của 400 = 40(size)
    return percent / 100.0 * MediaQuery.of(_context).size.height;
  }

  ///get Height of screen without app by percent
  double getHWithoutAppbar([double percent = 100]) {
    //lấy kích size dựa trên tỉ lệ của màn hình.
    //value là tỉ lệ phần trăm so với màn hình,
    // ví dụ value = 10, width = 400 =>  tương ứng 10% của 400 = 40(size)
    return percent /
        100.0 *
        (MediaQuery.of(_context).size.height -
            Scaffold.of(_context).appBarMaxHeight);
  }

  ///get Height of screen without appbar and bottombar by percent
  double getHWithoutAppbarAndBottomNaviBar([double percent = 100]) {
    //lấy kích size dựa trên tỉ lệ của màn hình.
    //value là tỉ lệ phần trăm so với màn hình,
    // ví dụ value = 10, width = 400 =>  tương ứng 10% của 400 = 40(size)
    return percent /
        100.0 *
        (MediaQuery.of(_context).size.height -
            Scaffold.of(_context).appBarMaxHeight -
            kBottomNavigationBarHeight);
  }
}
