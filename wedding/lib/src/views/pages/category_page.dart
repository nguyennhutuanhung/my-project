import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/category_item.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> _title = [
      'Trang điểm',
      'Trang phục',
      'Ảnh cưới',
      'Ảnh sự kiện',
      'Ảnh chân dung',
      'Ảnh món ăn',
      'Ảnh nội thất',
      'Other'
    ];
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
//          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: Text('Category'),
          actions: <Widget>[Icon(Icons.search)],
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: ListView.builder(
              itemCount: _title.length,
              itemBuilder: (context, index) {
                return CategoryItem(title: _title[index]);
              }),
        ));
  }
}
