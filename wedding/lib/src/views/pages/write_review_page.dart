import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/views/widgets/rating_box.dart';
import 'package:wedding/src/views/widgets/vendor_banner_in_review.dart';

class WriteReviewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Đánh giá',
            style: TextStyle(
                color: ThemeConfig.mainTitleTextColor,
                fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.clear),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Đăng',
                style: TextStyle(color: Color(0xffA84E4E)),
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              VendorBannerInReview(),
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 16),
                child: Text(
                  'Đánh giá',
                  style: TextStyle(
                      color: ThemeConfig.mainTitleTextColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 24),
                ),
              ),
              RatingBox(),
              Padding(
                padding: const EdgeInsets.only(top: 4, bottom: 16),
                child: Text(
                  'Đánh giá của bạn',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xffA84E4E),
                      fontSize: 18),
                ),
              ),
              Text(
                'Nhận xét',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                    color: ThemeConfig.mainTitleTextColor),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextFormField(
                  style: TextStyle(color: Color(0xffA84E4E)),
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: BorderSide(
                              color: Color(0xffA84E4E),
                              style: BorderStyle.solid)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: BorderSide(
                              color: Color(0xffA84E4E),
                              style: BorderStyle.solid)),
                      hintText: 'Viết nhận xét',
                      hintStyle: TextStyle(color: Color(0xffA84E4E))),
                  minLines: 8,
                  maxLines: 8,
                ),
              )
            ],
          ),
        ));
  }
}
