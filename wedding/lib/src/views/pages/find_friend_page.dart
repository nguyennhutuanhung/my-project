import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/people_tile.dart';

class FindFriendPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Tìm bạn'),
        ),
        body: Container(
          child: ListView.builder(
            itemBuilder: (context, index) {
              return PeopleTile();
            },
            itemCount: 20,
          ),
        ));
  }
}
