import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/body_bottom_bar.dart';
import 'package:wedding/src/views/widgets/bottom_bar.dart';
import 'package:wedding/src/views/widgets/category.dart';
import 'package:wedding/src/views/widgets/find_service.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/promotion.dart';
import 'package:wedding/src/views/widgets/scroll.dart';
import 'package:wedding/src/views/widgets/search_input.dart';
import 'package:wedding/src/views/widgets/services_update.dart';
import 'package:wedding/src/views/widgets/trending_vendor.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: SearchInput(),
          centerTitle: true,
        ),
        body: SafeArea(
          child: BodyBottomBar(
            body: bodyContent(),
          ),
        ));
  }

  Widget bodyContent() {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Container(
          color: Color(0xfffdfefd),
          child: Scroll(
            constraints: constraints,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  TrendingVendor(),
                  Category(),
                  FindService(),
                  Promotion(),
                  ServicesUpdate(),
                  Height(BottomBar.height)
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
