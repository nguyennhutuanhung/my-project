import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/rating_box.dart';
import 'package:wedding/src/views/widgets/select_range.dart';
import 'package:wedding/src/views/widgets/select_tag_category.dart';

class FilterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Bộ lọc'),
          actions: <Widget>[
            FlatButton(
              child: Icon(
                Icons.close,
                size: 36,
              ),
              onPressed: () {},
            )
          ],
        ),
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Chọn thể loại',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                    ),
                    Height(24),
                    Wrap(
                      children: <Widget>[
                        SelectTagCategory(
                          name: 'Photography',
                          active: true,
                        ),
                        SelectTagCategory(
                          name: 'Makeup',
                        ),
                        SelectTagCategory(
                          name: 'Dress',
                        ),
                        SelectTagCategory(
                          name: 'Photography',
                        ),
                        SelectTagCategory(
                          name: 'haha',
                        ),
                        SelectTagCategory(
                          name: 'haha',
                        ),
                        SelectTagCategory(
                          name: 'haha',
                        ),
                        SelectTagCategory(
                          name: 'haha',
                        ),
                        SelectTagCategory(
                          name: 'haha',
                        ),
                        SelectTagCategory(
                          name: 'haha',
                        )
                      ],
                    ),
                    Height(24),
                    Text('Distance',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24)),
                    Height(24),
                    SelectRange(
                      min: 0,
                      max: 100,
                      divisions: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '0',
                            style: TextStyle(color: Colors.pink),
                          ),
                          Text('100', style: TextStyle(color: Colors.pink))
                        ],
                      ),
                    ),
                    Text(
                      'Ratings',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                    ),
                    Height(24),
                    RatingBox()
                  ],
                ),
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: IntrinsicHeight(
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: FlatButton(
                              onPressed: () {},
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 24),
                                child: Text('Reset',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ),
                          Container(
                            width: 0.5,
                            color: Colors.white,
                          ),
                          Expanded(
                            child: FlatButton(
                              onPressed: () {},
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 24),
                                child: Text(
                                  'Apply',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Colors.pink,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(24),
                              topRight: Radius.circular(24))),
                    ),
                  ))
            ],
          ),
        ));
  }
}
