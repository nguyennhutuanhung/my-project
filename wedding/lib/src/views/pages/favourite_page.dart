import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/review_item_list.dart';
import 'package:wedding/src/views/widgets/trending_vendor.dart';
import 'package:wedding/src/views/widgets/vendor_banner.dart';

class FavouritePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Favourite'),
        ),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                  child: VendorBanner(),
                );
              },
              itemCount: 55,
            ),
          ),
        ));
  }
}
