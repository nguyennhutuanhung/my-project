import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/body_bottom_bar.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/search_input.dart';
import 'package:wedding/src/views/widgets/select_category.dart';
import 'package:wedding/src/views/widgets/select_range_date.dart';
import 'package:wedding/src/views/widgets/select_range_price.dart';
import 'package:wedding/src/views/widgets/width.dart';

class FindAdvance extends StatelessWidget {
  Widget bodyContent() {
    return Card(
        margin: const EdgeInsets.all(16),
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SearchInput(),
                Height(6),
                Text(
                  'Bạn cần tìm dịch vụ:',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Height(6),
                SelectCategory(),
                Height(6),
                SelectRangePrice(),
                Height(8),
                SelectCategory(),
                Height(6),
                Text(
                  'Thời gian sử dụng dịch vụ:',
                  style: TextStyle(
                      color: Colors.pink, fontWeight: FontWeight.bold),
                ),
                Height(6),
                SelectRangeDate(),
                Height(6),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        onPressed: () {},
                        color: Colors.pink,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Text('Tìm cao cấp',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                    Width(8),
                    Expanded(
                      child: RaisedButton(
                        onPressed: () {},
                        color: Colors.pink,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Text(
                          'Tiến hành',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffececec),
      appBar: AppBar(
        centerTitle: true,
        title: Text('Tìm kiếm nâng cao'),
      ),
      body: SafeArea(
        child: BodyBottomBar(
          body: bodyContent(),
        ),
      ),
    );
  }
}
