import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Test extends StatefulWidget {
  @override
  _TestState createState() => new _TestState();
}

class _TestState extends State<Test> {
  Widget logo = CircleAvatar(
    backgroundColor: Colors.transparent,
    radius: 80,
    backgroundImage: AssetImage('image/Logo.png'),
  );

  Widget email() => TextField(
        autofocus: true,
        textCapitalization: TextCapitalization.characters,
        onSubmitted: (value) {},
        decoration: InputDecoration(
          hintText: 'Mã sinh viên',
          labelText: 'Mã sinh viên',
          prefixIcon: Icon(Icons.account_circle),
          suffixIcon:
              IconButton(icon: Icon(Icons.arrow_forward), onPressed: () {}),
          contentPadding: EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 12.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
        ),
      );

  Widget password() => TextField(
        obscureText: true,
        onSubmitted: (value) {},
        decoration: InputDecoration(
          hintText: 'Mật khẩu',
          labelText: 'Mật khẩu',
          prefixIcon: Icon(Icons.lock),
          suffixIcon: IconButton(icon: Icon(Icons.check), onPressed: () {}),
          contentPadding: EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 12.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
        ),
      );

  Widget loginButton() => Container(
        width: double.infinity,
        height: 44,
        padding: const EdgeInsets.all(0),
        child: RaisedButton(
          child: Text('ĐĂNG NHẬP', style: TextStyle(color: Colors.white)),
          onPressed: () {},
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          color: Colors.green,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Column(
                children: <Widget>[
                  TextField(),
                  TextField(),
                  TextField(),
                  TextField(),
                  TextField(),
                  TextField(),
                  TextField(),
                  TextField(),
                  TextField(),
                ],
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: IntrinsicHeight(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Container(
                          color: Colors.green,
                          child: Text('body'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        child: Text('Bottom'),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
