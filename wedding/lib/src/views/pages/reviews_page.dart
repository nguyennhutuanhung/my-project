import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/review_item_list.dart';

class ReviewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Đánh giá'),
        ),
        body: Container(
          child: ListView.builder(
            itemBuilder: (context, index) {
              return ReviewItemList(
                index: index,
              );
            },
            itemCount: 55,
          ),
        ));
  }
}
