import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/scroll.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: Text('Quen mat khau'),
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Colors.transparent,
        ),
        body: LayoutBuilder(
          builder: (context, constraint) {
            return Stack(children: <Widget>[
              Container(
                constraints: BoxConstraints.expand(),
                child: Image.asset(
                  'assets/images/re2.png',
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.transparent, Colors.black])),
              ),
              Scroll(
                constraints: constraint,
                child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Column(
                    children: <Widget>[
                      Height(
                        100,
                      ),
                      Text(
                        'Ban hay nhap email cua ban o duoi day va he thong se gui email huong dan ban tao mat khau moi',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                      Height(
                        24,
                      ),
                      Container(
                        decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white38),
                        child: TextField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintText: 'Email',
                              hintStyle: TextStyle(color: Colors.white),
                              prefixIcon: Icon(
                                Icons.email,
                                color: Colors.white,
                              ),
                              border: InputBorder.none,
                              contentPadding: const EdgeInsets.all(16)),
                        ),
                      ),
                      Expanded(
                        child: SizedBox(),
                      ),
                      FractionallySizedBox(
                        widthFactor: 1.0,
                        child: RaisedButton(
                          elevation: 0,
                          padding: const EdgeInsets.all(14),
                          child: Text('Login'),
                          onPressed: () {},
                          color: Colors.pink,
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.red)),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ]);
          },
        ));
  }
}
