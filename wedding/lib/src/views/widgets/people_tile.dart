import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/helpers/sizes.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/width.dart';

class PeopleTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Sizes sizes = Sizes.set(context);
    return Container(
      margin: const EdgeInsets.fromLTRB(16, 16, 16, 24),
      child: Row(
        children: <Widget>[
          Container(
            width: sizes.getW(15),
            child: FittedBox(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/images/promo1.png'),
              ),
            ),
          ),
          Width(24),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Tên là minh',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: ThemeConfig.mainTitleTextColor),
                ),
                Height(8),
                Text(
                  '50 Reviews',
                  style: TextStyle(color: Color(0xffFF6589), fontSize: 16),
                )
              ],
            ),
          ),
          Container(
            width: sizes.getW(20),
            height: sizes.getW(8), // height =  2/5 of width
            child: Center(
              child: Text(
                'Follow',
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.pink,
              borderRadius: BorderRadius.circular(8),
            ),
          )
        ],
      ),
    );
  }
}
