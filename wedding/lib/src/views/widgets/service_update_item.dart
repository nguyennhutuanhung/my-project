import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';

import 'height.dart';

class ServiceUpdateItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.width / 4,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(6))),
                child: Image.asset(
                  'assets/images/promo1.png',
                  fit: BoxFit.fill,
                )),
          ],
        ),
        Height(2),
        Text(
          'Photography',
          style: ThemeConfig.titleStyle,
        )
      ],
    );
    ;
  }
}
