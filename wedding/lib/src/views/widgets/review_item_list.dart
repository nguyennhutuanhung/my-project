import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/width.dart';

class ReviewItemList extends StatelessWidget {
  final int index;

  ReviewItemList({this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(16, 16, 16, 24),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            radius: 24,
            child: Image.asset('assets/images/avatar.png'),
          ),
          Width(16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      'Name lakdj ajdfljal l',
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    )),
                    Container(
                      padding: const EdgeInsets.fromLTRB(6, 4, 6, 4),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Color(0x0dFF225E)),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Text(
                            '4.5',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Height(16),
                Text(
                  'Tớ tên là minh nhé hihijfljsdlf aslfjlsdjfl ljsdf  jdlkfj $index',
                  style: TextStyle(color: Color(0xccFF6589), fontSize: 16),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
