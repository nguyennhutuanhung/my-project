import 'package:flutter/material.dart';

class Height extends StatelessWidget {
  final double value;

  Height(this.value);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: value,
    );
  }
}
