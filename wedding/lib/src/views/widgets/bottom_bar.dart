import 'package:flutter/material.dart';
import 'package:wedding/src/views/pages/register.dart';

class BottomBar extends StatelessWidget {
  static const double height = 50;
  static const double radius = 24;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: BottomBar.height * 1.6,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: BottomBar.height,
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(BottomBar.radius),
                    topLeft: Radius.circular(BottomBar.radius)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Flexible(
                      flex: 1, child: Image.asset('assets/images/home.png')),
                  Flexible(
                      flex: 1, child: Image.asset('assets/images/tag.png')),
                  Flexible(
                    flex: 3,
                    child: SizedBox(),
                  ),
                  Flexible(
                      flex: 1,
                      child: Image.asset('assets/images/notification.png')),
                  Flexible(
                      flex: 1, child: Image.asset('assets/images/user.png')),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: FloatingActionButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return RegisterPage();
                  }));
                },
                child: Icon(
                  Icons.clear,
                  size: 30,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
