import 'package:flutter/material.dart';

class TagCategory extends StatelessWidget {
  final String name;

  TagCategory({@required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(4, 3, 4, 3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Color(0xffFF8C48), Color(0xffFF5673)])),
      child: Text(
        name,
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}
