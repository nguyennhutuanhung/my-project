import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/bottom_bar.dart';

class BodyBottomBar extends StatelessWidget {
  final Widget body;

  BodyBottomBar({this.body});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        body,
        Align(
          alignment: Alignment.bottomCenter,
          child: BottomBar(),
        ),
      ],
    );
  }
}
