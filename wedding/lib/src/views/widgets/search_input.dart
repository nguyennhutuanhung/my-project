import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wedding/src/views/pages/filter_page.dart';

class SearchInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xffe8e8e8)),
          borderRadius: BorderRadius.circular(8)),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Image.asset(
              'assets/images/search.png',
              width: 25,
            ),
          ),
          Expanded(
            child: TextField(
              autofocus: false,
              style: TextStyle(color: Color(0xffFF6589).withOpacity(0.8)),
              decoration: InputDecoration(
                  hintText: 'Tìm kiếm',
                  hintStyle:
                      TextStyle(color: Color(0xffFF6589).withOpacity(0.8)),
                  border: InputBorder.none),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Image.asset(
              'assets/images/filter.png',
              width: 25,
            ),
          )
        ],
      ),
    );
  }
}
