import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/views/pages/category_page.dart';
import 'package:wedding/src/views/widgets/category_item_home.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/width.dart';

class Category extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Thể loại',
                    style: ThemeConfig.titleStyle,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return CategoryPage();
                      }));
                    },
                    child: Text(
                      'Tất cả (9)',
                      style: TextStyle(
                          color: ThemeConfig.mainPinkColor,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
              Height(2),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(child: CategoryItemHome()),
                  Width(16),
                  Flexible(child: CategoryItemHome()),
                  Width(16),
                  Flexible(child: CategoryItemHome()),
                ],
              )
            ],
          ),
        ));
  }
}
