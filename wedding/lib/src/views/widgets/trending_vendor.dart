import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/list_icon.dart';
import 'package:wedding/src/views/widgets/tag.dart';
import 'package:wedding/src/views/widgets/tag_category.dart';
import 'package:wedding/src/views/widgets/vendor_banner.dart';
import 'package:wedding/src/views/widgets/width.dart';

class TrendingVendor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Trending Vendor',
                style: TextStyle(
                    color: ThemeConfig.mainTitleTextColor,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                'See all (45)',
                style: TextStyle(color: Colors.pink),
              )
            ],
          ),
        ),
        VendorBanner()
      ],
    );
  }
}
