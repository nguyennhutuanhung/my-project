import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wedding/src/views/widgets/width.dart';

class SelectRangeDate extends StatefulWidget {
  @override
  _SelectRangeDateState createState() => _SelectRangeDateState();
}

class _SelectRangeDateState extends State<SelectRangeDate> {
  DateTime _startTime = DateTime.now();
  DateTime _endTime = DateTime.now();
  DateFormat _format = DateFormat('dd/MM/yyyy');

  Future<DateTime> _selectedDate() async {
    return await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        RichText(
          text: TextSpan(
              text: 'Từ: ',
              style: TextStyle(color: Colors.black),
              children: [
                TextSpan(
                    text: _format.format(_startTime),
                    style: TextStyle(
                        color: Colors.pink, fontWeight: FontWeight.bold)),
                TextSpan(text: ' đến '),
                TextSpan(
                    text: _format.format(_endTime),
                    style: TextStyle(
                        color: Colors.pink, fontWeight: FontWeight.bold)),
              ]),
        ),
        Width(6),
        GestureDetector(
            onTap: () async {
              DateTime start = await _selectedDate();
              DateTime end = await _selectedDate();
              if (start != null) _startTime = start;
              if (end != null) _endTime = end;
              setState(() {});
            },
            child: Image.asset(
              'assets/images/date.png',
              width: 20,
            )),
      ],
    );
  }
}
