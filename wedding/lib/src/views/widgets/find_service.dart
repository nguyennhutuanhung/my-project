import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/views/pages/find_advance.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/select_category.dart';
import 'package:wedding/src/views/widgets/select_range_price.dart';
import 'package:wedding/src/views/widgets/width.dart';

class FindService extends StatefulWidget {
  @override
  _FindServiceState createState() => _FindServiceState();
}

class _FindServiceState extends State<FindService> {
  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Bạn cần tìm dịch vụ:',
                style: ThemeConfig.titleStyle,
              ),
              Height(6),
              SelectCategory(),
              Height(6),
              SelectRangePrice(),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(context,
                            CupertinoPageRoute(builder: (context) {
                          return FindAdvance();
                        }));
                      },
                      color: ThemeConfig.mainPinkColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: Text('Tìm cao cấp',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Width(8),
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {},
                      color: ThemeConfig.mainPinkColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: Text(
                        'Tiến hành',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
