import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';

import 'height.dart';

class PromotionItemHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.width / 4,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(6))),
                child: Image.asset(
                  'assets/images/promo1.png',
                  fit: BoxFit.fill,
                )),
            Positioned(
              top: 8,
              right: 8,
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                    color: ThemeConfig.mainPinkColor,
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: Center(
                    child: Text(
                  '-25%',
                  style: TextStyle(color: Colors.white, fontSize: 10),
                )),
              ),
            )
          ],
        ),
        Height(2),
        Text(
          'Photography',
          style: ThemeConfig.titleStyle,
        )
      ],
    );
    ;
  }
}
