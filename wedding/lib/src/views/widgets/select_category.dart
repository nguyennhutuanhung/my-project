import 'package:flutter/material.dart';

class SelectCategory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[Text('Chọn thể loại'), Icon(Icons.arrow_drop_down)],
      ),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xffececec)),
          borderRadius: BorderRadius.all(Radius.circular(8))),
      padding: const EdgeInsets.all(4),
    );
  }
}
