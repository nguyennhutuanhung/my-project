import 'package:flutter/material.dart';

class Width extends StatelessWidget {
  final double value;

  Width(this.value);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: value,
    );
  }
}
