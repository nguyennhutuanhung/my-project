import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';

import 'height.dart';
import 'list_icon.dart';
import 'tag.dart';
import 'tag_category.dart';
import 'width.dart';

class VendorBannerInReview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      elevation: 4,
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Image.asset(
                'assets/images/trending_background.png',
                fit: BoxFit.fill,
              ),
              Positioned(
                top: 8,
                right: 8,
                child: InkWell(
                    onTap: () {},
                    child: Image.asset(
                      'assets/images/cancel_circle.png',
                      width: 32,
                      height: 32,
                    )),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Tên nhà cung cấp',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color(0xff3E3F68)),
                        ),
                        Width(48),
                        TagCategory(
                          name: 'Photography',
                        ),
                      ],
                    ),
                    Height(6),
                    Text(
                      '123 Sương Nguyệt Anh, P Bến Thành, HCM',
                      style: TextStyle(color: Color(0xffA84E4E)),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: _addedButton(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _addButton() {
    return CircleAvatar(
      backgroundColor: ThemeConfig.mainPinkColor,
      child: Icon(
        Icons.add,
        color: Colors.white,
      ),
    );
  }

  Widget _addedButton() {
    return CircleAvatar(
      backgroundColor: Color(0xff4CD964),
      child: Icon(
        Icons.check,
        color: Colors.white,
      ),
    );
  }
}
