import 'package:flutter/material.dart';

class SelectTagCategory extends StatefulWidget {
  final String name;
  final bool active;

  SelectTagCategory({this.name, this.active});

  @override
  _SelectTagCategoryState createState() => _SelectTagCategoryState();
}

class _SelectTagCategoryState extends State<SelectTagCategory> {
  bool _isActive;

  void initState() {
    super.initState();
    _isActive = widget.active ?? false;
  }

  EdgeInsets _getPadding() {
    if (widget.name.length > 6) {
      return EdgeInsets.fromLTRB(8, 12, 8, 12);
    } else {
      return EdgeInsets.fromLTRB(28, 12, 28, 12);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_isActive) {
      return Opacity(
        opacity: 0.65,
        child: GestureDetector(
          onTap: () {
            setState(() {
              _isActive = !_isActive;
            });
          },
          child: Container(
            padding: _getPadding(),
            margin: const EdgeInsets.all(4),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Colors.grey, width: 0.5),
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Color(0xffFF7E53), Color(0xffFF5673)])),
            child: Text(
              widget.name,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      );
    } else {
      return GestureDetector(
        onTap: () {
          setState(() {
            _isActive = !_isActive;
          });
        },
        child: Container(
          padding: _getPadding(),
          margin: const EdgeInsets.all(4),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(color: Colors.grey, width: 0.5),
          ),
          child: Text(
            widget.name,
            style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
          ),
        ),
      );
    }
  }
}
