import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wedding/src/config/theme.dart';

class SelectRangePrice extends StatefulWidget {
  @override
  _SelectRangePriceState createState() => _SelectRangePriceState();
}

class _SelectRangePriceState extends State<SelectRangePrice> {
  var selectedRange = RangeValues(0, 10000000);
  final formater = new NumberFormat("#,###", "en_US");

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Chọn khoảng giá',
          style: ThemeConfig.titleStyleWith(ThemeConfig.mainPinkColor),
        ),
        RangeSlider(
          values: selectedRange,
          onChanged: (RangeValues value) {
            setState(() {
              selectedRange = value;
            });
          },
          min: 0,
          max: 10000000,
          divisions: 10,
          activeColor: ThemeConfig.mainPinkColor,
          labels: RangeLabels(formater.format(selectedRange.start),
              formater.format(selectedRange.end)),
        ),
        RichText(
          text: TextSpan(
              text: 'Từ: ',
              style: TextStyle(color: Colors.black),
              children: [
                TextSpan(
                    text: formater.format(selectedRange.start),
                    style:
                        ThemeConfig.titleStyleWith(ThemeConfig.mainPinkColor)),
                TextSpan(text: ' đến '),
                TextSpan(
                    text: formater.format(selectedRange.end),
                    style:
                        ThemeConfig.titleStyleWith(ThemeConfig.mainPinkColor)),
                TextSpan(text: ' VNĐ')
              ]),
        ),
      ],
    );
  }
}
