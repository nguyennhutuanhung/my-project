import 'package:flutter/material.dart';

class ListIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 48,
      height: 20,
      child: Stack(
        children: <Widget>[
          Positioned(
              top: 0,
              right: 0,
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  child: Icon(
                    Icons.account_circle,
                    color: Colors.green,
                    size: 20,
                  ))),
          Positioned(
              top: 0,
              right: 10,
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  child: Icon(
                    Icons.account_circle,
                    color: Colors.purple,
                    size: 20,
                  ))),
          Positioned(
              top: 0,
              right: 20,
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  child: Icon(
                    Icons.account_circle,
                    color: Colors.red,
                    size: 20,
                  ))),
          Positioned(
              top: 0,
              right: 30,
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(50))),
                  child: Icon(
                    Icons.account_circle,
                    color: Colors.yellow,
                    size: 20,
                  ))),
        ],
      ),
    );
  }
}
