import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/views/widgets/service_update_item.dart';

import 'height.dart';
import 'width.dart';

class ServicesUpdate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Cập nhật từ danh sách',
                      style: ThemeConfig.titleStyleWith(
                          ThemeConfig.mainPinkColor)),
                  Text(
                    'Tất cả (9)',
                    style:
                        ThemeConfig.titleStyleWith(ThemeConfig.mainPinkColor),
                  )
                ],
              ),
              Height(4),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(child: ServiceUpdateItem()),
                  Width(16),
                  Flexible(child: ServiceUpdateItem()),
                ],
              ),
              Height(2),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(child: ServiceUpdateItem()),
                  Width(16),
                  Flexible(child: ServiceUpdateItem())
                ],
              )
            ],
          ),
        ));
    ;
  }
}
