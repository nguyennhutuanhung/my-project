import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/views/widgets/promotion_item_home.dart';
import 'package:wedding/src/views/widgets/width.dart';

import 'height.dart';

class Promotion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('KHUYẾN MẠI TRONG NGÀY',
                      style: ThemeConfig.titleStyleWith(
                          ThemeConfig.mainPinkColor)),
                  Text(
                    'Tất cả (9)',
                    style:
                        ThemeConfig.titleStyleWith(ThemeConfig.mainPinkColor),
                  )
                ],
              ),
              Height(4),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(child: PromotionItemHome()),
                  Width(16),
                  Flexible(child: PromotionItemHome()),
                ],
              ),
              Height(2),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(child: PromotionItemHome()),
                  Width(16),
                  Flexible(child: PromotionItemHome())
                ],
              )
            ],
          ),
        ));
    ;
  }
}
