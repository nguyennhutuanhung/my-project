import 'package:flutter/material.dart';

class SelectRange extends StatefulWidget {
  final double min, max;
  final int divisions;

  SelectRange({this.min, this.max, this.divisions});

  @override
  _SelectRangeState createState() => _SelectRangeState();
}

class _SelectRangeState extends State<SelectRange> {
  RangeValues selectedRange;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedRange = RangeValues(widget.min, widget.max);
  }

  @override
  Widget build(BuildContext context) {
    return RangeSlider(
      values: selectedRange,
      onChanged: (RangeValues value) {
        setState(() {
          selectedRange = value;
        });
      },
      min: widget.min,
      max: widget.max,
      divisions: widget.divisions,
      labels: RangeLabels(selectedRange.start.toInt().toString(),
          selectedRange.end.toInt().toString()),
    );
  }
}
