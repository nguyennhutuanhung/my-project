import 'package:flutter/material.dart';
import 'package:wedding/src/views/widgets/height.dart';
import 'package:wedding/src/views/widgets/list_icon.dart';
import 'package:wedding/src/views/widgets/tag.dart';
import 'package:wedding/src/views/widgets/tag_category.dart';
import 'package:wedding/src/views/widgets/width.dart';

class VendorBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      elevation: 4,
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Image.asset(
                'assets/images/trending_background.png',
                fit: BoxFit.fill,
              ),
              Positioned(
                top: 8,
                left: 8,
                child: Container(
                  height: 24,
                  child: FittedBox(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0),
                      ),
                      color: Colors.white,
                      child: Text(
                        'OPEN',
                        style: TextStyle(color: Colors.green),
                      ),
                      onPressed: () {},
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 8,
                right: 8,
                child: Container(
                  height: 24,
                  child: FittedBox(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0),
                      ),
                      color: Colors.white,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Text(
                            '4.5',
                          ),
                        ],
                      ),
                      onPressed: () {},
                    ),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Tên nhà cung cấp',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: <Widget>[
                        TagCategory(
                          name: 'Photography',
                        ),
                        Width(2),
                        Tag(
                          name: '1.2 km',
                        ),
                      ],
                    ),
                    ListIcon()
                  ],
                ),
                Height(6),
                Text(
                  '123 Sương Nguyệt Anh, P Bến Thành, HCM',
                  style: TextStyle(color: Color(0xffA84E4E)),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
