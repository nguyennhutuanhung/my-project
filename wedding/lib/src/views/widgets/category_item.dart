import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryItem extends StatelessWidget {
  final title;

  CategoryItem({this.title});

  List<Color> _colors = [
    Colors.pink,
    Colors.orange,
    Colors.red,
    Colors.purple,
    Colors.blue,
    Colors.cyan
  ];

  List<Color> _getColors() {
    return [
      _colors[Random().nextInt(_colors.length)],
      _colors[Random().nextInt(_colors.length)]
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 16),
      height: 70,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Stack(
          children: <Widget>[
            SizedBox.expand(
                child: Image.asset(
              'assets/images/category_item_background.png',
              fit: BoxFit.cover,
            )),
            Opacity(
              opacity: 0.9,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: _getColors(), tileMode: TileMode.mirror),
                ),
              ),
            ),
            Center(
                child: Text(
              title,
              style: TextStyle(color: Colors.white, fontSize: 24),
            ))
          ],
        ),
      ),
    );
  }
}
