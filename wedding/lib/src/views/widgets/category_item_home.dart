import 'package:flutter/material.dart';
import 'package:wedding/src/config/theme.dart';
import 'package:wedding/src/views/widgets/height.dart';

class CategoryItemHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1,
          child: Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(6))),
              child: Image.asset(
                'assets/images/category1.png',
                fit: BoxFit.fill,
              )),
        ),
        Height(2),
        Text(
          'Photography',
          style: ThemeConfig.titleStyle,
        )
      ],
    );
  }
}
