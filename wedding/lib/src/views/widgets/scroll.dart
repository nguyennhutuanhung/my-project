import 'package:flutter/material.dart';

class Scroll extends StatelessWidget {
  final Widget child;
  final BoxConstraints constraints;
  final bool reverse;

  Scroll({@required this.constraints, @required this.child, this.reverse});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      reverse: reverse ?? false,
      child: ConstrainedBox(
        constraints: BoxConstraints(
            minHeight: constraints.maxHeight, minWidth: constraints.maxWidth),
        child: IntrinsicHeight(child: child),
      ),
    );
  }
}
