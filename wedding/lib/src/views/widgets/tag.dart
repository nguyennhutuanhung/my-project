import 'package:flutter/material.dart';

class Tag extends StatelessWidget {
  final String name;

  Tag({@required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(8, 3, 8, 3),
      decoration: BoxDecoration(
        color: Colors.pink,
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      child: Text(
        name,
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}
